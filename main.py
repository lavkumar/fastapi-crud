from fastapi import Depends, FastAPI,status,HTTPException
from jsonschema import SchemaError
from matplotlib.pyplot import title
from requests import Session, session
from database import engine,SessionLocal
import models
from schemas import ArticleBase,myarticle
from sqlalchemy.orm import Session
from typing import List



models.Base.metadata.create_all(bind=engine)

app = FastAPI()

#dependency
def get_db():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()




@app.get("/")
def root():
    return {"message": "Hello World"}

@app.get('/article/',response_model=List[myarticle])
def get_article(db:Session = Depends(get_db)):
    myarticle=db.query(models.Article).all()
    return myarticle

@app.get('/article/{id}',status_code=status.HTTP_200_OK,response_model=myarticle)
def article_details(id:int,db:Session=Depends(get_db)):
   # myarticle=db.query(models.Article).filter(models.Article.id == id).first()
    myarticle=db.query(models.Article).get(id)
    if myarticle:
        return myarticle

    raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,detail="the articles are not exits")
     



@app.post('/article/', status_code=status.HTTP_201_CREATED)
def add_article(article:ArticleBase,db:Session = Depends(get_db)):
     new_article = models.Article(title=article.title,description=article.description)
     db.add(new_article)
     db.commit()
     db.refresh(new_article)
     return new_article


@app.put('/article/{id}',status_code=status.HTTP_202_ACCEPTED)
def update_article(id,article:ArticleBase,db:Session = Depends(get_db)):
    db.query(models.Article).filter(models.Article.id == id).update({'title':article.title,'description':article.description})
    db.commit()
    return {'data update'}

@app.delete('/article/{id}',status_code=status.HTTP_204_NO_CONTENT)
def delete_article(id:int,db:Session = Depends(get_db)):
    db.query(models.Article).filter(models.Article.id == id).delete(synchronize_session=False)
    db.commit()
    return {'deleted'}




